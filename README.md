# Coding Challenge
### Overview
Create a tool to query the Amazon Simple Storage Service (S3). You can create a free Amazon account [here](http://aws.amazon.com/en/free/).
### Specifications
Create a CLI that retrieves information from [S3 buckets](https://aws.amazon.com/documentation/s3/) in your Amazon account.
##### The tool must return the following information:
- Bucket name
- Creation date (of the bucket)
- Number of files
- Total size of files
- Last modified date (most recent file of a bucket)

##### The following options should be supported:
- Return a list of results sorted by size
- Organize the information by [storage type](https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-class-intro.html) (Standard, IA, Glacier, RRS)
- Filter the results by bucket name
- Statistics - used space, count and total size of files added per month. Bonus points - estimate usage for the next month.

### Constraints
- You are free to use any technology of your choice - here is a list of official [SDKs](https://aws.amazon.com/tools/#sdk)
- Fork this repo for your project.
- Your tool should be able to run against large S3 buckets (with millions of files).
- Some notes inside a `README` would be appreciated:
    * Brief description of the tool.
    * Prerequisites for building (if needed) and installing your tool.
    * Documentation on running - command-line parameters, output, etc.
    * Any constraints or limitation of note.
    * Why did you choose the technology/framework you did and how much experience do you have with it?

## Bonus points
- Proper error handling.
- Don't you forget those unit tests.
- Feel free to add anything you can think of.
